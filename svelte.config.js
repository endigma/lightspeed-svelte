import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter({
			pages: 'dist',
			fallback: 'index.html'
		}),
		vite: {
			envPrefix: 'WEBSOCKET'
		}
	},

	preprocess: [
		preprocess({
			postcss: true
		})
	]
};

export default config;
