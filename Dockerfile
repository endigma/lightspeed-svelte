ARG ALPINE_VERSION=3.15
ARG NODE_VERSION=16

FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION} AS builder
WORKDIR /app/lightspeed-svelte
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:stable-alpine
COPY --from=builder /app/lightspeed-svelte/dist /usr/share/nginx/html
EXPOSE 80/tcp
