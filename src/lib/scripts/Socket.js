import { variables } from "$lib/variables";

export class Socket {
    url = variables.wsUrl;
    socket = null;
    wsTimeoutDuration = 250;
    connectTimeout = null;

    constructor() {
        this.socket = new WebSocket(this.url);
        this.addEventListeners();
    }

    renewSocket() {
        let timeout = this.wsTimeoutDuration * 2;
        if (timeout > 10000) {
          timeout = 10000;
        }
        this.wsTimeoutDuration = timeout;
        this.socket = new WebSocket(this.url);
        this.addEventListeners();
    }

    tryReconnect() {
        if (!this.socket || this.socket.readyState === WebSocket.CLOSED) {
            this.renewSocket();
            setTimeout(this.tryReconnect, this.wsTimeoutDuration);
        }
    }

    addEventListeners() {
        this.socket.addEventListener('open', () => {
            console.log("socket connected");
            this.wsTimeoutDuration = 250;
            this.connectTimeout = null;
        });
        this.socket.addEventListener('close', (e) => {
            let reason = e;
            let timeoutSeconds = Math.min(wsTimeoutDuration / 1000);
            console.log(`socket closed, retrying in ${timeoutSeconds} ${timeoutSeconds > 1 ? "seconds" : "second"}. Reason:`);
            console.log(e);
            this.tryReconnect();
        });
        this.socket.addEventListener('onerror', (e) => {
            console.error("error, closing socket");
            console.error(e);
            socket.close();
        });
    }
}
