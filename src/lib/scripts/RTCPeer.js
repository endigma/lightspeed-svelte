export class PeerContext {
	pc = new RTCPeerConnection();
	socket = null;
	stream = null;
	connections = null;

	constructor(socket) {
		// Offer to receive 1 audio, and 1 video tracks
		this.pc.addTransceiver('audio', { direction: 'recvonly' });
		this.pc.addTransceiver('video', { direction: 'recvonly' });
		this.addEventListeners();
		this.socket = socket;

		if (this.socket) {
			this.socket.socket.addEventListener('message', (e) => {
				const msg = JSON.parse(e.data);
				if (!msg) {
					console.error('failed to parse msg');
					return;
				}

				const offerCandidate = msg.data;
				if (!offerCandidate) {
					console.error('failed to parse offer msg data');
					return;
				}

				switch (msg.event) {
					case 'offer':
						console.log('got offer');
						this.pc.setRemoteDescription(offerCandidate);

						try {
							const answer = this.pc.createAnswer().then(answer => {
								this.pc.setLocalDescription(answer);
								this.socket.socket.send(
									JSON.stringify({
										event: 'answer',
										data: answer
									})
								);	
							});
						} catch (e) {
							console.error(e.message);
						}

						return;
					case 'candidate':
						console.log('got candidate');
						this.pc.addIceCandidate(offerCandidate);
						return;
					case 'info':
						let event = new CustomEvent("gotViewers");
						window.dispatchEvent(event);		
						this.connections = msg.data.no_connections
				}
			});
		}
	}

	addEventListeners() {
		this.pc.ontrack = (e) => {
			if (e.track.kind === 'video') {
				this.stream = e.streams[0];
				let event = new CustomEvent("gotStream");
				window.dispatchEvent(event);
			}
		};
		this.pc.onicecandidate = (e) => {
			if (e.candidate) {
				this.socket.socket.send(
					JSON.stringify({
						event: 'candidate',
						data: e.candidate
					})
				);
			}
		};
	}
}
