export const variables = {
    wsHost: import.meta.env.WEBSOCKET_HOST,
    wsPort: import.meta.env.WEBSOCKET_PORT,
    wsSchema: import.meta.env.WEBSOCKET_SCHEMA,
    wsUrl: `${import.meta.env.WEBSOCKET_SCHEMA}://${import.meta.env.WEBSOCKET_HOST}:${import.meta.env.WEBSOCKET_PORT}/websocket`
};
